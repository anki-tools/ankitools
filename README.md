# AnkiTools

Little command line program for manipulation Anki throught console.

## Instalation

1. Install *anki-connect* plugin
2. Download and build from source code

## Features

### Card templates

You can create and use templates to speed up the creation of new cards
