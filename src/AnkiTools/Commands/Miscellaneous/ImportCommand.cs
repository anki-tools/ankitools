﻿using System;
using System.CommandLine;
using System.CommandLine.Invocation;
using System.IO;
using AnkiNet;
using AnkiNet.Miscellaneous;

namespace AnkiTools.Commands.Miscellaneous
{
    public class ImportCommand : Command
    {
        public ImportCommand(AnkiClient client) : base("import", "Imports a file in .apkg format into the collection.")
        {
            if (client == null)
                throw new ArgumentNullException(nameof(client));

            AddArgument(new Argument("path")
            {
                ArgumentType = typeof(FileInfo),
            });

            Handler = CommandHandler.Create<FileInfo>(async path =>
            {
                var result = await client.ImportPackageAsync(path.ToString());
                if (result.Error != null)
                    Console.WriteLine(result.Error);
            });
        }
    }
}