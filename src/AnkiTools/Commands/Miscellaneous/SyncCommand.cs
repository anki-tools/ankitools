﻿using System;
using System.CommandLine;
using System.CommandLine.Invocation;
using AnkiNet;
using AnkiNet.Miscellaneous;

namespace AnkiTools.Commands.Miscellaneous
{
    public class SyncCommand : Command
    {
        public SyncCommand(AnkiClient client) : base("sync", "Send sync command to Anki connect.")
        {
            if (client == null)
                throw new ArgumentNullException(nameof(client));

            Handler = CommandHandler.Create(async () => await client.SyncAsync());
        }
    }
}