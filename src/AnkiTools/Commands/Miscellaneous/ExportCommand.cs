﻿using System;
using System.CommandLine;
using System.CommandLine.Invocation;
using System.IO;
using AnkiNet;
using AnkiNet.Miscellaneous;

namespace AnkiTools.Commands.Miscellaneous
{
    public class ExportCommand : Command
    {
        public ExportCommand(AnkiClient client) : base("export", "Exports a given deck in .apkg format.")
        {
            if (client == null)
                throw new ArgumentNullException(nameof(client));

            AddArgument(new Argument("deck")
            {
                ArgumentType = typeof(string),
            });
            AddArgument(new Argument("path")
            {
                ArgumentType = typeof(FileInfo),
            });
            AddOption(new Option<bool>("--no-shed"));
            Handler = CommandHandler.Create<string, FileInfo, bool>(async (deck, path, noShed) =>
            {
                var result = await client.ExportPackageAsync(deck, path.ToString(), !noShed);
                if (result.Error != null)
                    Console.WriteLine(result.Error);
            });
        }
    }
}