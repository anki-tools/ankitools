﻿using System;
using System.CommandLine;
using System.CommandLine.Invocation;
using AnkiNet;
using AnkiNet.Deck;

namespace AnkiTools.Commands.Deck
{
    public class ListDeckCommand : Command
    {
        public ListDeckCommand(AnkiClient client) : base("list", "Print all decks.")
        {
            if (client == null)
                throw new ArgumentNullException(nameof(client));

            Handler = CommandHandler.Create(async () =>
            {
                var result = await client.DeckNamesAsync();
                if (result.Error != null)
                    Console.WriteLine(result.Error);
                if (result.Result != null)
                    Console.WriteLine(string.Join("\n", result.Error));
            });
        }
    }
}