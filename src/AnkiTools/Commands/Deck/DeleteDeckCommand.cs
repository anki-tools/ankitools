﻿using System;
using System.Collections.Generic;
using System.CommandLine;
using System.CommandLine.Invocation;
using AnkiNet;
using AnkiNet.Deck;

namespace AnkiTools.Commands.Deck
{
    public class DeleteDeckCommand : Command
    {
        public DeleteDeckCommand(AnkiClient client) : base("delete", "Delete decks.")
        {
            if (client == null)
                throw new ArgumentNullException(nameof(client));

            AddArgument(new Argument("decks")
            {
                Arity = new ArgumentArity(1, 16),
                Description = "Decks to delete.",
                ArgumentType = typeof(IEnumerable<string>),
            });
            Handler = CommandHandler.Create<IEnumerable<string>>(async decks =>
            {
                var result = await client.DeleteDecksAsync(decks, true);
                if (result.Error != null)
                    Console.WriteLine(result.Error);
            });
        }
    }
}