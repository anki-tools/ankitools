﻿using System;
using System.CommandLine;
using System.CommandLine.Invocation;
using AnkiNet;
using AnkiNet.Deck;

namespace AnkiTools.Commands.Deck
{
    public class CreateDeckCommand : Command
    {
        public CreateDeckCommand(AnkiClient client) : base("create", "Create new deck.")
        {
            if (client == null)
                throw new ArgumentNullException(nameof(client));

            AddArgument(new Argument("deck"));

            Handler = CommandHandler.Create<string>(async deck =>
            {
                var result = await client.CreateDeckAsync(deck);
                if (result.Error != null)
                    Console.WriteLine(result.Error);
            });
        }
    }
}