﻿using System;
using System.CommandLine;
using System.CommandLine.Invocation;
using AnkiNet;
using AnkiNet.Model;

namespace AnkiTools.Commands.Model
{
    public class FieldsModelCommand : Command
    {
        public FieldsModelCommand(AnkiClient client) : base("fields",
            "Gets the complete list of field names for the provided model name.")
        {
            if (client == null)
                throw new ArgumentNullException(nameof(client));

            AddArgument(new Argument("model"));

            Handler = CommandHandler.Create<string>(async model =>
            {
                var result = await client.ModelFieldNamesAsync(model);
                if (result.Error != null)
                    Console.WriteLine(result.Error);
                if (result.Result != null)
                    Console.WriteLine(string.Join("\n", result.Result));
            });
        }
    }
}