﻿using System;
using System.CommandLine;
using System.CommandLine.Invocation;
using AnkiNet;
using AnkiNet.Model;

namespace AnkiTools.Commands.Model
{
    public class ListModelCommand : Command
    {
        public ListModelCommand(AnkiClient client) : base("list",
            "Gets the complete list of model names for the current user.")
        {
            if (client == null)
                throw new ArgumentNullException(nameof(client));

            Handler = CommandHandler.Create(async () =>
            {
                var result = await client.ModelNamesAsync();
                if (result.Error != null)
                    Console.WriteLine(result.Error);
                if (result.Result != null)
                    Console.WriteLine(string.Join("\n", result.Result));
            });
        }
    }
}