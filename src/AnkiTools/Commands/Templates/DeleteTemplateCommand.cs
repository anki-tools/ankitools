﻿using System.CommandLine;
using System.CommandLine.Invocation;
using System.IO;

namespace AnkiTools.Commands.Templates
{
    public class DeleteTemplateCommand : Command
    {
        public DeleteTemplateCommand() : base("delete", "Delete template.")
        {
            AddArgument(new Argument("name"));
            Handler = CommandHandler.Create<string>(name =>
            {
                var path = Path.Combine("templates", $"{name}.html");
                if (File.Exists(path))
                    File.Delete(path);
            });
        }
    }
}