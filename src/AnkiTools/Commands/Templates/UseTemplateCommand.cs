﻿using System;
using System.CommandLine;
using System.CommandLine.Invocation;
using System.IO;
using System.Threading.Tasks;
using AnkiNet;
using AnkiNet.Notes;
using AnkiTools.Extensions;
using Microsoft.Extensions.Configuration;

namespace AnkiTools.Commands.Templates
{
    public class UseTemplateCommand : Command
    {
        private readonly AnkiClient _client;
        private readonly IConfiguration _configuration;

        public UseTemplateCommand(AnkiClient client, IConfiguration configuration) : base("use",
            "Create note with selected template.")
        {
            _client = client ?? throw new ArgumentNullException(nameof(client));
            _configuration = configuration ?? throw new ArgumentNullException(nameof(configuration));

            AddArgument(new Argument("name"));

            Handler = CommandHandler.Create<string>(async name => await UseTemplateCommandHandlerAsync(name));
        }

        private async Task UseTemplateCommandHandlerAsync(string name)
        {
            var templatePath = Path.Combine("templates", $"{name}.html");
            if (!File.Exists(templatePath))
            {
                Console.WriteLine($"fail: template with name \"{name}\" not found.");
                Environment.Exit(-1);
            }

            var path = Path.ChangeExtension(Path.GetRandomFileName(), ".html");
            File.Copy(templatePath, path);
            await _configuration.RunEditor(path);
            var note = await CreateTemplateCommand.ParseNoteAsync(path);
            (await _client.AddNoteAsync(note)).EnsureError(() => File.Delete(path));
            File.Delete(path);
        }
    }
}