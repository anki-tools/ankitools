﻿using System;
using System.CommandLine;
using System.CommandLine.Invocation;
using System.IO;

namespace AnkiTools.Commands.Templates
{
    public class ListTemplateCommand : Command
    {
        public ListTemplateCommand() : base("list", "Show all templates.")
        {
            Handler = CommandHandler.Create(() =>
            {
                if (Directory.Exists("templates"))
                    foreach (var file in Directory.GetFiles("templates"))
                        if (file.EndsWith(".html"))
                            Console.WriteLine(Path.GetFileNameWithoutExtension(file));
            });
        }
    }
}