﻿using System;
using System.Collections.Generic;
using System.CommandLine;
using System.CommandLine.Invocation;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using AnkiNet;
using AnkiNet.Model;
using AnkiNet.Notes;
using AnkiTools.Extensions;
using Microsoft.Extensions.Configuration;

namespace AnkiTools.Commands.Templates
{
    public class CreateTemplateCommand : Command
    {
        private readonly AnkiClient _client;
        private readonly IConfiguration _configuration;

        public CreateTemplateCommand(AnkiClient client, IConfiguration configuration) : base("create",
            "Create new template and open editor.")
        {
            _client = client ?? throw new ArgumentNullException(nameof(client));
            _configuration = configuration ?? throw new ArgumentNullException(nameof(configuration));

            AddArgument(new Argument("name"));
            AddOption(new Option<string?>(new[] {"--model", "-m"}));
            Handler = CommandHandler.Create<string, string?>(async (name, model) =>
                await CreateTemplateAsync(name, model));
        }

        private async Task CreateTemplateAsync(string name, string? model)
        {
            if (!Directory.Exists("templates"))
                Directory.CreateDirectory("templates");

            var template = new FileInfo(Path.Combine("templates", $"{name}.html"));
            if (template.Exists)
            {
                Console.WriteLine($"fail: template with name \"{name}\" already exists.");
                Environment.Exit(-1);
            }

            if (model == null)
                await template.Create().DisposeAsync();
            else
                await CreateTemplateViaModelAsync(_client, model, template.ToString());

            await _configuration.RunEditor(template.ToString());
        }

        internal static async Task<AnkiNet.Notes.Note> ParseNoteAsync(string fileName)
        {
            using var streamReader = File.OpenText(fileName);
            var builder = new NoteBuilder();
            var state = "";
            var directives = new Dictionary<string, string[]>();
            var sb = new StringBuilder();
            while (!streamReader.EndOfStream)
            {
                var line = await streamReader.ReadLineAsync();
                if (!string.IsNullOrEmpty(line) && line.StartsWith("<!--") && line.EndsWith("-->"))
                {
                    if (sb.Length > 0 && state != "Tags" && state != "Deck")
                        builder.AddField(state, sb.ToString());

                    state = line.Substring(4, line.Length - 7);
                    sb.Clear();
                    continue;
                }

                if (!string.IsNullOrEmpty(line))
                {
                    if (line[0] == '@')
                    {
                        var directive = line.Split();
                        directives.Add(directive[0][1..].ToLower(), directive[1..]);
                        continue;
                    }

                    if (state == "Tags")
                        builder.AddTag(line);
                    if (state == "Deck")
                        builder.SetDeckName(line);
                    else
                        sb.AppendLine(line);
                }
            }

            if (!directives.ContainsKey("model") || directives["model"].Length < 1)
            {
                Console.WriteLine($"Can't found \"model\" directive in {fileName}.");
                Environment.Exit(-1);
            }

            return builder.SetModelName(directives["model"][0]).Build();
        }

        internal static async Task<string> CreateTemplateViaModelAsync(AnkiClient client, string model, string path)
        {
            var result = (await client.ModelFieldNamesAsync(model)).EnsureError();
            await using var writer = File.CreateText(path);
            await writer.WriteLineAsync($"@model {model}");

            if (result.Result != null)
                foreach (var filed in result.Result)
                    await writer.WriteLineAsync($"<!--{filed}-->\n");
            await writer.WriteLineAsync("<!--Deck-->\n");
            await writer.WriteAsync("<!--Tags-->");
            return path;
        }
    }
}