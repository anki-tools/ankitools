﻿using System.CommandLine;

namespace AnkiTools.Commands
{
    public class ParentCommand : Command
    {
        public ParentCommand(string name, string? description = null) : base(name, description)
        {
        }

        public ParentCommand(string name, string? description = null, params Command[]? subCommands) : base(name,
            description)
        {
            if (subCommands != null)
                foreach (var subCommand in subCommands)
                    AddCommand(subCommand);
        }
    }
}