﻿using System;
using System.CommandLine;
using System.CommandLine.Invocation;
using System.IO;
using System.Threading.Tasks;
using AnkiNet;
using AnkiNet.Notes;
using AnkiTools.Commands.Templates;
using AnkiTools.Extensions;
using Microsoft.Extensions.Configuration;

namespace AnkiTools.Commands.Note
{
    public class CreateNoteCommand : Command
    {
        private readonly AnkiClient _client;
        private readonly IConfiguration _configuration;

        public CreateNoteCommand(AnkiClient client, IConfiguration configuration) : base("create",
            "Creates a note using the given deck and model, with the provided field values and tags.")
        {
            _client = client ?? throw new ArgumentNullException(nameof(client));
            _configuration = configuration ?? throw new ArgumentNullException(nameof(configuration));
            AddArgument(new Argument("model"));
            Handler = CommandHandler.Create<string>(async model => await CreateNoteCommandHandler(model));
        }

        private async Task CreateNoteCommandHandler(string model)
        {
            var path = await CreateTemplateCommand.CreateTemplateViaModelAsync(_client, model,
                Path.ChangeExtension(Path.GetRandomFileName(), ".html"));
            await _configuration.RunEditor(path);
            var note = await CreateTemplateCommand.ParseNoteAsync(path);
            (await _client.AddNoteAsync(note)).EnsureError(() => File.Delete(path));
            File.Delete(path);
        }
    }
}