﻿using System;
using System.CommandLine;
using System.CommandLine.Builder;
using System.CommandLine.Parsing;
using System.IO;
using System.Threading.Tasks;
using AnkiNet;
using AnkiTools.Commands;
using AnkiTools.Commands.Deck;
using AnkiTools.Commands.Miscellaneous;
using AnkiTools.Commands.Model;
using AnkiTools.Commands.Note;
using AnkiTools.Commands.Templates;
using AnkiTools.Extensions;
using Microsoft.Extensions.Configuration;

namespace AnkiTools
{
    internal sealed class Program
    {
        private static async Task Main(string[] args)
        {
            var client = new AnkiClient();
            var root = new RootCommand("Tool for operating with Anki through console.");
            Environment.CurrentDirectory = Path.GetDirectoryName(typeof(Program).Assembly.Location) ??
                                           Environment.CurrentDirectory;
            var builder = new CommandLineBuilder(root);
            ConfigureCommands(root, client, await ConfigureConfiguration());
            await builder.UseDefaults()
                         .UseAnkiConnectExceptionHandler()
                         .Build()
                         .InvokeAsync(args);
        }

        private static async Task<IConfiguration> ConfigureConfiguration()
        {
            var configuration = new ConfigurationBuilder().AddJsonFile("config.json").Build();
            if (!File.Exists("config.json"))
                await File.WriteAllTextAsync("config.json", "{\n}");

            return configuration;
        }

        private static void ConfigureCommands(RootCommand rootCommand, AnkiClient client, IConfiguration configuration)
        {
            rootCommand.AddCommand(new SyncCommand(client));
            rootCommand.AddCommand(new ParentCommand("deck", "Manipulating decks.",
                new CreateDeckCommand(client),
                new DeleteDeckCommand(client),
                new ListDeckCommand(client)
            ));
            rootCommand.AddCommand(new ExportCommand(client));
            rootCommand.AddCommand(new ImportCommand(client));
            rootCommand.AddCommand(new ParentCommand("model", "Manipulating models.",
                new ListModelCommand(client),
                new FieldsModelCommand(client)
            ));
            rootCommand.AddCommand(new ParentCommand("note", "Manipulating notes.",
                new CreateNoteCommand(client, configuration)
            ));
            rootCommand.AddCommand(new ParentCommand("template", "Manipulating templates.",
                new CreateTemplateCommand(client, configuration),
                new UseTemplateCommand(client, configuration),
                new DeleteTemplateCommand(),
                new ListTemplateCommand()
            ));
        }
    }
}