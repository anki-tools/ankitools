﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;

namespace AnkiTools.Extensions
{
    public static class ConfigurationExtensions
    {
        public static async Task RunEditor(this IConfiguration configuration, string arguments)
        {
            if (!configuration.GetSection("editor").Exists())
            {
                Console.WriteLine("fail: Add editor path to config.json file.");
                Environment.Exit(-1);
            }

            var processInfo = new ProcessStartInfo(configuration["editor"], arguments);
            var process = Process.Start(processInfo);
            await process?.WaitForExitAsync()!;
            Console.WriteLine("Hit enter for continue...");
            Console.ReadLine();
        }
    }
}