﻿using System;
using AnkiNet;

namespace AnkiTools.Extensions
{
    public static class CommandResultExtensions
    {
        public static CommandResult<T> EnsureError<T>(this CommandResult<T> commandResult, Action? onError = null)
        {
            if (commandResult.Error != null)
            {
                Console.WriteLine(commandResult.Error);
                onError?.Invoke();
                Environment.Exit(-1);
            }

            return commandResult;
        }
    }
}