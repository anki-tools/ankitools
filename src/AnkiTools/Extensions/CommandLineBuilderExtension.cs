﻿using System.CommandLine.Builder;
using System.CommandLine.IO;
using AnkiNet;

namespace AnkiTools.Extensions
{
    public static class CommandLineBuilderExtension
    {
        public static CommandLineBuilder UseAnkiConnectExceptionHandler(this CommandLineBuilder builder)
        {
            return builder.UseExceptionHandler((exception, context) =>
            {
                if (exception is AnkiConnectException ankiConnectException)
                {
                    context.Console.Error.WriteLine($"fail: {ankiConnectException.Exception.Message}");
                    if (context.ParseResult.Directives.Contains("r"))
                    {
                        context.Console.Error.WriteLine($"request: {ankiConnectException.Request}");
                        context.Console.Error.WriteLine($"response: {ankiConnectException.Response}");
                    }
                }
                else
                {
                    context.Console.Error.WriteLine(exception.ToString());
                }
            });
        }
    }
}